drop table ACTIVITES_PRATIQUEES ;
drop table CHARGE ;
drop table RESULTATS ;
drop table ACTIVITES ;
drop table PROFESSEURS ;
drop table COURS ;
drop table ELEVES ;

-- ============================================================
ALTER SESSION SET NLS_DATE_FORMAT = 'DD-MM-YYYY' ;
-- ============================================================

-- ============================================================
--   Table : ELEVES                                            
-- ============================================================
create table ELEVES
(
    NUM_ELEVE       Number (4)   NOT NULL,
    NOM             VARCHAR2(25) NOT NULL,
    PRENOM          VARCHAR2(25) NOT NULL,
    DATE_NAISSANCE  DATE,
    POIDS           Number,
    ANNEE           Number,
    constraint PK_ELEVES primary key (NUM_ELEVE)
);

-- ============================================================
--   Table : COURS                                             
-- ============================================================
create table COURS
(
    NUM_COURS       Number(2)   NOT NULL,
    NOM             VARCHAR(20) NOT NULL,
    NBHEURES        Number(2),
    ANNEE           Number(1),
    constraint PK_COURS primary key (NUM_COURS)
);
-- ============================================================
--   Table : PROFESSEURS                                       
-- ============================================================
create table PROFESSEURS
(
    NUM_PROF        Number(4)    NOT NULL,
    NOM             VARCHAR2(25) NOT NULL,
    SPECIALITE      VARCHAR2(20),
    DATE_ENTREE     DATE,
    DER_PROM        DATE,
    SALAIRE_BASE    Number,
    SALAIRE_ACTUEL  Number,
    constraint PK_PROFESSEURS primary key (NUM_PROF)
);

-- ============================================================
--   Table : ACTIVITES                                         
-- ============================================================
create table ACTIVITES
(
    NIVEAU          Number(1)    NOT NULL,
    NOM             VARCHAR2(20) NOT NULL,
    EQUIPE          VARCHAR2(32),
    constraint PK_ACTIVITES primary key (NIVEAU,NOM)
);
-- ============================================================
--   Table : RESULTATS                                         
-- ============================================================
create table RESULTATS
(
    NUM_ELEVE       Number(4) NOT NULL,
    NUM_COURS       Number(4) NOT NULL,
    POINTS         Number,
    constraint PK_RESULTATS primary key (NUM_ELEVE, NUM_COURS)
);

-- ============================================================
--   Table : CHARGE                                            
-- ============================================================
create table CHARGE
(
    NUM_PROF       Number(4) NOT NULL,
    NUM_COURS      Number(4) NOT NULL,
    constraint PK_CHARGE primary key (NUM_COURS, NUM_PROF)
);

-- ============================================================
--   Table : ACTIVITES_PRATIQUEES                              
-- ============================================================
create table ACTIVITES_PRATIQUEES
(
    NUM_ELEVE       Number(4) NOT NULL,
    NIVEAU          Number(1) NOT NULL,
    NOM             VARCHAR2(20) NOT NULL,
    constraint PK_ACTIVITES_PRATIQUEES primary key 
    (NUM_ELEVE, NIVEAU, NOM) 
);
-- ============================================================
--   Les cles �trangeres                                 
-- ============================================================

alter table RESULTATS
    add constraint FK_RESULTAT_ELEVES foreign key  (NUM_ELEVE)
       references ELEVES (NUM_ELEVE);

alter table RESULTATS
    add constraint FK_RESULTAT_COURS foreign key  (NUM_COURS)
       references COURS (NUM_COURS);

alter table CHARGE
    add constraint FK_CHARGE_COURS foreign key  (NUM_COURS)
       references COURS (NUM_COURS);

alter table CHARGE
    add constraint FK_CHARGE_PROFESSEUR foreign key  (NUM_PROF)
       references PROFESSEURS (NUM_PROF);

alter table ACTIVITES_PRATIQUEES
    add constraint FK_ACTIVITE_ELEVES foreign key  (NUM_ELEVE)
       references ELEVES (NUM_ELEVE):

alter table ACTIVITES_PRATIQUEES
    add constraint FK_ACTIVITEPR_ACTIVITE foreign key  (NOM, NIVEAU)
       references ACTIVITES (NOM, NIVEAU);


REM **************************************************************************
REM Creation des donnees
REM **************************************************************************

SELECT 'Creation des donnees' FROM DUAL;
SELECT 'Tapez sur RETURN 'FROM DUAL;
PAUSE


Insert into eleves (Num_eleve, nom, prenom, date_naissance, Poids, annee)
Values (1, 'Brisefer', 'Benoit', '10-12-1978', 35,1) ;

Insert into eleves (Num_eleve, nom, prenom, date_naissance, Poids, annee)
Values (2, 'G�nial', 'Olivier', '10-04-1978', 42, 1) ;

Insert into eleves (num_eleve, nom, prenom, date_naissance, Poids, annee)
Values (3, 'Jourdan', 'Gil', '28-06-1974', 72, 2) ;

Insert into eleves (num_eleve, nom, prenom, date_naissance, Poids, annee)
Values (4, 'Spring', 'Jerry', '16-02-1974', 78, 2) ;

Insert into eleves (num_eleve, nom, prenom, date_naissance, Poids, annee)
Values (5, 'Tsuno', 'Yoko', '29-10-1977', 45, 1) ;

Insert into eleves (num_eleve, nom, prenom, date_naissance, Poids, annee)
Values (6, 'Lebut', 'Marc', '29-04-1974', 75,2) ;

Insert into eleves (num_eleve, nom, prenom, date_naissance, Poids, annee)
Values (7, 'Lagaffe', 'Gaston', '08-04-1975', 61,1) ;

Insert into eleves (num_eleve, nom, prenom, date_naissance, Poids, annee)
Values (8, 'Dubois', 'Robin', '20-04-1976', 60, 2) ;

Insert into eleves (num_eleve, nom, prenom, date_naissance, Poids, annee)
Values (9, 'Walth�ry', 'Natacha', '07-09-1977', 59,1) ;

Insert into eleves (num_eleve, nom, prenom, date_naissance, Poids, annee)
Values (10, 'Danny', 'Buck', '15-02-1973', 82, 2) ;


Insert into cours (Num_cours, Nom, Nbheures, annee)
Values (1, 'R�seau', 15, 1);

Insert into cours (Num_cours, Nom, Nbheures, annee)
Values (2, 'Sgbd', 30, 1) ;

Insert into cours (Num_cours, Nom, Nbheures, annee)
Values (3, 'Programmation', 15,1) ;

Insert into cours (Num_cours, Nom, Nbheures, annee)
Values (4, 'Sgbd', 30,2 ) ;

Insert into cours (Num_cours, Nom, Nbheures, annee)
Values (5, 'Analyse', 60,2) ;


Insert into PROFESSEURS (Num_prof, nom , specialite, Date_entree, Der_prom, Salaire_base, Salaire_actuel)
Values(1, 'Bottle', 'po�sie', '01-10-1970', '01-10-1988', 2000000, 2600000) ;

Insert into PROFESSEURS (Num_prof, nom , specialite, Date_entree, Der_prom, Salaire_base, Salaire_actuel)
Values(2, 'Bolenov', 'r�seau', '15-11-1968', '01-10-1998', 1900000, 2468000) ;

Insert into PROFESSEURS (Num_prof, nom , specialite, Date_entree, Der_prom, Salaire_base, Salaire_actuel)
Values(3, 'Tonilaclasse', 'poo', '01-10-1979', '01-01-1989', 1900000, 2360000) ;

Insert into PROFESSEURS (Num_prof, nom , specialite, Date_entree, Der_prom, Salaire_base, Salaire_actuel)
Values(4, 'Pastecnov', 'sql', '01-10-1975', '', 2500000, 2500000) ;

Insert into PROFESSEURS (Num_prof, nom , specialite, Date_entree, Der_prom, Salaire_base, Salaire_actuel)
Values(5, 'Selector', 'sql', '15-10-1982', '01-10-1988', 1900000, 1900000) ;

Insert into PROFESSEURS (Num_prof, nom , specialite, Date_entree, Der_prom, Salaire_base, Salaire_actuel)
Values(6, 'Vilplusplus', 'poo', '25-04-1990', '05-06-1994', 1900000, 2200000) ;

Insert into PROFESSEURS (Num_prof, nom , specialite, Date_entree, Der_prom, Salaire_base, Salaire_actuel)
Values(7, 'Francesca', '', '01-10-1975', '11-01-1998', 2000000, 3200000) ;

Insert into PROFESSEURS (Num_prof, nom , specialite, Date_entree, Der_prom, Salaire_base, Salaire_actuel)
Values(8, 'Pucette', 'sql', '06-12-1988', '29-02-1996', 2000000, 2500000) ;

Insert into CHARGE (Num_prof, Num_cours)
Values(1,1) ;

Insert into CHARGE (Num_prof, Num_cours)
Values(1,4) ;

Insert into CHARGE (Num_prof, Num_cours)
Values(2,1) ;

Insert into CHARGE (Num_prof, Num_cours)
Values(3,2) ;

Insert into CHARGE (Num_prof, Num_cours)
Values(3,4) ;

Insert into CHARGE (Num_prof, Num_cours)
Values(3,5) ;

Insert into CHARGE (Num_prof, Num_cours)
Values(4,2) ;

Insert into CHARGE (Num_prof, Num_cours)
Values(7,4) ;

Insert into CHARGE (Num_prof, Num_cours)
Values(8,1) ;

Insert into CHARGE (Num_prof, Num_cours)
Values(8,2) ;

Insert into CHARGE (Num_prof, Num_cours)
Values(8,3) ;

Insert into CHARGE (Num_prof, Num_cours)
Values(8,4) ;

Insert into CHARGE (Num_prof, Num_cours)
Values(8,5) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(1,1, 15 ) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(1,2 ,10.5 ) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(1, 4, 8) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(1, 5, 20) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(2, 1, 13.5) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(2, 2, 12) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(2, 4, 11) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(2, 5, 1.5) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(3, 1, 14) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(3, 2, 15) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(3, 4, 16) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(3, 5, 20) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(4, 1, 16.5) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(4, 2, 14) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(4, 4, 11) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(4, 5, 8) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(5, 1, 5) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(5, 2, 6.5) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(5, 4, 13) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(5, 5, 13) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(6, 1, 15) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(6, 2, 3.5) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(6, 4, 16) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(6, 5, 5) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(7, 1, 2.5) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(7, 2, 18) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(7, 4, 11) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(7, 5, 10) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(8, 1, 16) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(8, 2, 0) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(8, 4, 6) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(8, 5, 11.5) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(9, 1, 20) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(9, 2, 20) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(9, 4, 14) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(9, 5, 9.5) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(10, 1, 3) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(10, 2, 12.5) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(10, 4, 0) ;

Insert into RESULTATS (Num_eleve, Num_cours, points)
Values(10, 5, 16) ;

Insert into ACTIVITES (Niveau, Nom, equipe)
Values(1,'Mini foot','Amc Indus') ;

Insert into ACTIVITES (Niveau, Nom, equipe) 
values (1,'Surf','Les planchistes ...') ;


Insert into ACTIVITES (Niveau, Nom, equipe)
Values(2,'Tennis','Ace Club') ;

Insert into ACTIVITES (Niveau, Nom, equipe)
Values(3,'Tennis','Ace Club') ;

Insert into ACTIVITES (Niveau, Nom, equipe)
Values(1,'Volley ball', 'Avs80') ;

Insert into ACTIVITES (Niveau, Nom, equipe)
Values(2,'Mini foot', 'Les as du ballon') ;

Insert into ACTIVITES (Niveau, Nom, equipe)
Values(2,'Volley ball', 'smash') ;

Insert into ACTIVITES_PRATIQUEES (Num_eleve, Niveau, Nom)
Values (1, 1, 'Mini foot') ;

Insert into ACTIVITES_PRATIQUEES (Num_eleve, Niveau, Nom)
Values (1, 1, 'Surf') ;

Insert into ACTIVITES_PRATIQUEES (Num_eleve, Niveau, Nom)
Values (1, 1, 'Volley ball') ;

Insert into ACTIVITES_PRATIQUEES (Num_eleve, Niveau, Nom)
Values (1, 2, 'Tennis') ;

Insert into ACTIVITES_PRATIQUEES (Num_eleve, Niveau, Nom)
Values (2, 1, 'Mini foot') ;

Insert into ACTIVITES_PRATIQUEES (Num_eleve, Niveau, Nom)
Values (2, 2, 'Tennis') ;

Insert into ACTIVITES_PRATIQUEES (Num_eleve, Niveau, Nom)
Values (3, 2, 'Mini foot') ;

Insert into ACTIVITES_PRATIQUEES (Num_eleve, Niveau, Nom)
Values (3, 2, 'Tennis') ;

Insert into ACTIVITES_PRATIQUEES (Num_eleve, Niveau, Nom)
Values (3, 2, 'Volley ball') ;

Insert into ACTIVITES_PRATIQUEES (Num_eleve, Niveau, Nom)
Values (4, 1, 'Surf') ;

Insert into ACTIVITES_PRATIQUEES (Num_eleve, Niveau, Nom)
Values (4, 2, 'Mini foot') ;

Insert into ACTIVITES_PRATIQUEES (Num_eleve, Niveau, Nom)
Values (5, 1, 'Mini foot') ;

Insert into ACTIVITES_PRATIQUEES (Num_eleve, Niveau, Nom)
Values (5, 1, 'Surf') ;

Insert into ACTIVITES_PRATIQUEES (Num_eleve, Niveau, Nom)
Values (5, 1, 'Volley ball') ;

Insert into ACTIVITES_PRATIQUEES (Num_eleve, Niveau, Nom)
Values (8, 1, 'Mini foot') ;

Insert into ACTIVITES_PRATIQUEES (Num_eleve, Niveau, Nom)
Values (8, 1, 'Volley ball') ;

Insert into ACTIVITES_PRATIQUEES (Num_eleve, Niveau, Nom)
Values (8, 2, 'Volley ball') ;

Insert into ACTIVITES_PRATIQUEES (Num_eleve, Niveau, Nom)
Values (9, 1, 'Mini foot') ;

Insert into ACTIVITES_PRATIQUEES (Num_eleve, Niveau, Nom)
Values (9, 2, 'Volley ball') ;

Insert into ACTIVITES_PRATIQUEES (Num_eleve, Niveau, Nom)
Values (10, 1, 'Mini foot') ;

Insert into ACTIVITES_PRATIQUEES (Num_eleve, Niveau, Nom)
Values (10, 2, 'Tennis') ;

Insert into ACTIVITES_PRATIQUEES (Num_eleve, Niveau, Nom)
Values (10, 2, 'Volley ball') ;

REM ********************************************************************************

REM ********************************************************************************

COMMIT;
REM *************************************************

SELECT * FROM ELEVES ;
SELECT * FROM PROFESSEURS ;
SELECT * FROM COURS ;
SELECT * FROM ACTIVITES ;
SELECT * FROM CHARGE ;
SELECT * FROM RESULTATS ;
SELECT * FROM ACTIVITES_PRATIQUEES ;


TTITLE 'LES ELEVES :'
COLUMN NOM FORMAT A10
COLUMN PRENOM FORMAT A10
SELECT * FROM ELEVES ;

TTITLE 'LES PROFESSEURS :'
COLUMN NOM FORMAT A15
SELECT * FROM PROFESSEURS ;

TTITLE 'LES COURS :'
COLUMN NOM FORMAT A15
SELECT * FROM COURS ;

TTITLE 'LES ACTIVITES :'
SELECT * FROM ACTIVITES ;

TTITLE 'LES CHARGES :'
SELECT * FROM CHARGE ;

TTITLE 'LES RESULTATS :'
SELECT * FROM RESULTATS ;

TTITLE 'LES ACTIVITES PRATIQUEES:'
SELECT * FROM ACTIVITES_PRATIQUEES ;


REM *************************************************

REM *************************************************

PAUSE
SELECT * FROM ELEVES ;
PAUSE
DESC ELEVES;
ALTER TABLE ELEVES ADD (CODEPOSTAL NUMBER(5), VILLE VARCHAR(20));
DESC ELEVES;
COLUMN NOM FORMAT A10
COLUMN PRENOM FORMAT A10
SELECT * FROM ELEVES ;

REM *************************************************

UPDATE ELEVES
SET CODEPOSTAL = 93800
WHERE NUM_ELEVE = 1;

UPDATE ELEVES
SET VILLE = 'EPINAY / seine'
WHERE NUM_ELEVE = 1;

UPDATE ELEVES
SET CODEPOSTAL = 75013
WHERE NUM_ELEVE = 2;

UPDATE ELEVES
SET VILLE = 'paris'
WHERE NUM_ELEVE = 2;

UPDATE ELEVES
SET CODEPOSTAL = 93800
WHERE NUM_ELEVE = 5;

UPDATE ELEVES
SET VILLE = 'EPINAY SUR SEINE'
WHERE NUM_ELEVE = 5;

UPDATE ELEVES
SET CODEPOSTAL = 91000
WHERE NUM_ELEVE = 7;

UPDATE ELEVES
SET VILLE = 'EPINAY SUR ORGE'
WHERE NUM_ELEVE = 7;
commit;

COLUMN NOM FORMAT A10
COLUMN PRENOM FORMAT A10
SELECT * FROM ELEVES ;
PAUSE
REM *************************************************

CREATE TABLE AGGLOMERATION 
( CODEPOSTAL NUMBER(5) PRIMARY KEY,
  VILLE VARCHAR(20) CHECK (VILLE = UPPER(VILLE)));

INSERT INTO AGGLOMERATION VALUES (75001, 'PARIS');
INSERT INTO AGGLOMERATION VALUES (75013, 'PARIS');
INSERT INTO AGGLOMERATION VALUES (93800, 'EPINAY SUR SEINE');
INSERT INTO AGGLOMERATION VALUES (93430, 'Villetaneuse');
INSERT INTO AGGLOMERATION VALUES (91000, 'EPINAY SUR ORGE');
commit;
SELECT * FROM AGGLOMERATION;
PAUSE

REM *************************************************

REM NETTOYAGE DE LA TABLE ELEVE

UPDATE ELEVES
SET VILLE = (SELECT VILLE FROM AGGLOMERATION 
WHERE ELEVES.CODEPOSTAL=AGGLOMERATION.CODEPOSTAL);
commit;
COLUMN NOM FORMAT A10
COLUMN PRENOM FORMAT A10
SELECT * FROM ELEVES ;
PAUSE
REM ********************************************************************************

REM ********************************************************************************

REM ********************************************************************************

REM ********************************************************************************
REM ***************************************************************************
