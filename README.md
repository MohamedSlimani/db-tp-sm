# README #

This is the Database TPs of the 2nd year CS in UDL-SBA 2016-2017
if you see this share it and if you have anything to add it will be appreciated. 

### Work enviranment ###

the TP work environment is [Oracle Database Express Edition 11g](http://www.oracle.com/technetwork/database/database-technologies/express-edition/downloads/index.html)

### How do I get set up? ###

After you set UP the work environment
to import the data in the SQL files just type :

```
#!SQL

@ <the path to the file>
```

 
Example :

```
#!SQL

@ data.sql
```

the documents with the files are the TP questions

### Who do I talk to? ### 

Repo owner is [Mohamed Slimani](https://www.facebook.com/Med.Slimani93)