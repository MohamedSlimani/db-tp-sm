
-- supp de tables --
drop table emp;
drop table dept;
drop table salgrade;

-- creation de tables --
create table dept(
dno number(1),
dnom varchar2(15),
dir number(2),
ville varchar2(15),
dsuper number(1),
constraint pk_dept primary key (dno)
);

create table emp(
ENO number(2),
ENOM varchar2(10),
PROF varchar2(15),
DATEEMB date,
SAL number(4),
COMM number(4),
DNO number(1),
constraint pk_emp primary key (eno),
constraint fk_emp_dept foreign key (dno) references dept
);

create table salgrade(
 GRADE NUMBER(1),
 LOSAL NUMBER(4),
 HISAL NUMBER(4)
);

-- insertion de données  --

insert into dept values (1, 'Commercial', 30, 'New York', NULL);
insert into dept values (2, 'Production', 20, 'Houston',1);
insert into dept values (3, 'Développement', 40, 'Boston',1);
insert into dept values (4, 'Conception', NULL, 'Boston',3);

alter session set nls_date_format='dd.mm.yy';

insert into emp values (10, 'Joe', 'Ingenieur',   '1.10.93', 3000, 3000, 3);
insert into emp values (20, 'Jack', 'Technicien', '1.5.88',  3000, 2000, 2);
insert into emp values (30, 'Jim',  'Vendeur',    '1.3.80',  5000, 5000, 1);
insert into emp values (40, 'Lucy', 'Ingenieur',  '11.4.80', 4500, 5000, 3);
insert into emp values (50, 'Lucy', 'Concepteur', '1.6.85',  3500, 5000, NULL);
insert into emp values (60, 'Djamel', 'Enseignant',    '15.4.82', 4500, 2500, 2);

insert into salgrade values (1,        700,       1200);
insert into salgrade values (2,       1201,       1400);
insert into salgrade values (3,       1401,       2000);
insert into salgrade values (4,       2001,       3000);
insert into salgrade values (5,       3001,       9999);



